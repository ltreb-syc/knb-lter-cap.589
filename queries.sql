# macroinvertebrates

-- postgres was not available programatically at the time of construction so
-- the data were harvested via ssh and harvested as files

-- need to repair bizarre strings in note_invertebrate_sample that are
-- confounding result format

-- fixes 248 but not 226 !
SELECT *, REGEXP_REPLACE(note_invertebrate_sample, '\s{2,}', '; ', 'g') FROM invertebrate_samples ;

-- fixes 248 and 226 !
SELECT
  *,
  REGEXP_REPLACE(note_invertebrate_sample, '\n', '; ', 'g')
FROM invertebrate_samples 
WHERE note_invertebrate_sample IS NOT NULL;

-- fixes 248 and 226 !
UPDATE invertebrate_samples SET note_invertebrate_sample = REGEXP_REPLACE(note_invertebrate_sample, '\n', '; ', 'g'); 

SELECT
  -- invertebrate_samples.id,
  sites.site_code,
  reaches.reach_code,
  surveyors.sur_name,
  surveyors.given_name,
  invertebrate_samples.year,
  invertebrate_samples.month,
  invertebrate_samples.day,
  invertebrate_samples.replicate,
  invertebrate_samples.note_invertebrate_sample,
  invertebrate_observations.count,
  invertebrate_observations.length,
  invertebrate_observations.subsample_fraction,
  invertebrate_observations.note_invertebrate_observation,
  invertebrate_taxa.display
FROM
  ltreb_syc.invertebrate_samples
LEFT JOIN ltreb_syc.sites ON sites.id = invertebrate_samples.site_id
LEFT JOIN ltreb_syc.reaches ON reaches.id = invertebrate_samples.reach_id
LEFT JOIN ltreb_syc.surveyors ON surveyors.id = invertebrate_samples.surveyor_id
JOIN ltreb_syc.invertebrate_observations ON invertebrate_observations.invertebrate_sample_id = invertebrate_samples.id
JOIN ltreb_syc.invertebrate_taxa ON invertebrate_taxa.id = invertebrate_observations.invertebrate_taxon_id
;

SELECT sites.site_code, reaches.reach_code, surveyors.sur_name, surveyors.given_name, invertebrate_samples.year, invertebrate_samples.month, invertebrate_samples.day, invertebrate_samples.replicate, invertebrate_samples.note_invertebrate_sample, invertebrate_observations.count, invertebrate_observations.length, invertebrate_observations.subsample_fraction, invertebrate_observations.note_invertebrate_observation, invertebrate_taxa.display FROM invertebrate_samples LEFT JOIN sites ON sites.id = invertebrate_samples.site_id LEFT JOIN reaches ON reaches.id = invertebrate_samples.reach_id LEFT JOIN surveyors ON surveyors.id = invertebrate_samples.surveyor_id JOIN invertebrate_observations ON invertebrate_observations.invertebrate_sample_id = invertebrate_samples.id JOIN invertebrate_taxa ON invertebrate_taxa.id = invertebrate_observations.invertebrate_taxon_id


\COPY (SELECT sites.site_code, reaches.reach_code, surveyors.sur_name, surveyors.given_name, invertebrate_samples.year, invertebrate_samples.month, invertebrate_samples.day, invertebrate_samples.replicate, invertebrate_samples.note_invertebrate_sample, invertebrate_observations.count, invertebrate_observations.length, invertebrate_observations.subsample_fraction, invertebrate_observations.note_invertebrate_observation, invertebrate_taxa.display FROM invertebrate_samples LEFT JOIN sites ON sites.id = invertebrate_samples.site_id LEFT JOIN reaches ON reaches.id = invertebrate_samples.reach_id LEFT JOIN surveyors ON surveyors.id = invertebrate_samples.surveyor_id JOIN invertebrate_observations ON invertebrate_observations.invertebrate_sample_id = invertebrate_samples.id JOIN invertebrate_taxa ON invertebrate_taxa.id = invertebrate_observations.invertebrate_taxon_id) TO 'macroinvertebrate_observations.csv' DELIMITER ',' CSV HEADER ;

scp prod.postgresql:/home/ubuntu/macroinvertebrate_observations.csv /home/srearl/Dropbox/development/knb-lter-cap.589/macroinvertebrate_observations.csv

## taxa table

SELECT
  invertebrate_taxa.phyllum,
  invertebrate_taxa.subphyllum,
  invertebrate_taxa.class,
  invertebrate_taxa.order,
  invertebrate_taxa.suborder,
  invertebrate_taxa.family,
  invertebrate_taxa.subfamily,
  invertebrate_taxa.genus,
  invertebrate_taxa.species,
  invertebrate_taxa.display,
  invertebrate_taxa.authority
FROM
  invertebrate_taxa
;

SELECT invertebrate_taxa.phyllum, invertebrate_taxa.subphyllum, invertebrate_taxa.class, invertebrate_taxa.order, invertebrate_taxa.suborder, invertebrate_taxa.family, invertebrate_taxa.subfamily, invertebrate_taxa.genus, invertebrate_taxa.species, invertebrate_taxa.display, invertebrate_taxa.authority FROM invertebrate_taxa ;


\COPY (SELECT invertebrate_taxa.phyllum, invertebrate_taxa.subphyllum, invertebrate_taxa.class, invertebrate_taxa.order, invertebrate_taxa.suborder, invertebrate_taxa.family, invertebrate_taxa.subfamily, invertebrate_taxa.genus, invertebrate_taxa.species, invertebrate_taxa.display, invertebrate_taxa.authority FROM invertebrate_taxa) TO 'macroinvertebrate_taxa.csv' DELIMITER ',' CSV HEADER ;

scp prod.postgresql:/home/ubuntu/macroinvertebrate_taxa.csv /home/srearl/Dropbox/development/knb-lter-cap.589/macroinvertebrate_taxa.csv
