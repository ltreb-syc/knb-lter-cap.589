# standard operating procedure for sampling and processing macroinvertebrates

## field protocol

1. Choose an area of stream with riffle, free of large rocks. 
2. Place core (see *sample collecting and data reporting* for dimension) open side down into sediment 10 cm (marked on side of core)
3. Using work glove, move gravel beside core to get your hand under the core. Pick up sediment and dump into pitcher.
4. Elutriate samples by filling pitcher with stream water, stirring, then dumping water back into core with mesh on the bottom. Repeat this step 6 times.
5. All sample water should be poured through mesh. 
6. If mesh becomes clogged, pat (spank) the bottom of the mesh to move water through
7. The objective is to keep everything that did not pass through the mesh. Move contents on mesh into a Whirl-Pak bag by emptying the pitcher, filling with water, and pouring some water through the opposite side of the mesh, pouring contents into a labeled Whirl-Pak bag held on the open side of core. Use as little water as possible.
8. Use enough ethanol to create a 70% ethanol solution.
9. Leave some air in the bag, hold ties, and whirl the bag around to close. Back pressure of air helps seal the bag. Twist ties together.

## sample processing

- Samples are coarsely sorted by students using dissecting microscopes; insects are placed in scintillation vials with 70% ethanol.
- A resident insect taxonomist identifies all arthropod specimens using a WILD M3Z stereo microscope to the level of family or finer if possible; counts the number of each morphotype specimen and, in some cases, measures their body lengths. A subsample of morphotype specimens is used when the number of individuals is large. For sub-sampling, each morphotype was divided equally using a sectioning circle. Sub-sampled specimens were counted and measured up to 50 individuals.

## sample collecting and data reporting

- Collecting core size
  + a 7-cm diameter core was used to collect samples from 2010-01-01 through 2014-03-11
  + a 10-cm diameter core was used to collect samples prior to 2010-01-01 and after 2014-03-12
- When sub-sampled, the number (`count`) of organisms should be multiplied by the `subsample_fraction` to get the total number of morphotype specimens.
